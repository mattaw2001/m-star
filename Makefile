.SUFFIXES: .html .adoc

.adoc.html:
	asciidoc $<

SOURCES = $(shell ls web/*.adoc)

all: $(SOURCES:.adoc=.html)

clean:
	rm -f *.html web/*.html m-star-logo.svg

upload: all
	git push

fixme:
	@if command -v rg; then \
		rg --no-heading FIX''ME; \
	else \
		find . -type f -exec grep -n FIX''ME {} /dev/null \; | grep -v "[.]git"; \
	fi
