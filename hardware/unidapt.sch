EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr USLetter 11000 8500
encoding utf-8
Sheet 1 1
Title "M-Star Mini"
Date "2021-07-29"
Rev "v0.02"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector_Generic:Conn_01x04 J4
U 1 1 5E891730
P 6700 5000
F 0 "J4" H 6780 4992 50  0000 L CNN
F 1 "LEDs" H 6780 4901 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 6700 5000 50  0001 C CNN
F 3 "~" H 6700 5000 50  0001 C CNN
	1    6700 5000
	1    0    0    -1  
$EndComp
$Comp
L Device:R R8
U 1 1 5E920DCC
P 5000 4900
F 0 "R8" V 4950 5050 50  0000 C CNN
F 1 "100" V 5000 4900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4930 4900 50  0001 C CNN
F 3 "~" H 5000 4900 50  0001 C CNN
F 4 "C17408" H 5000 4900 50  0001 C CNN "LCSC"
	1    5000 4900
	0    1    1    0   
$EndComp
$Comp
L Device:R R9
U 1 1 5E923217
P 5000 5200
F 0 "R9" V 4950 5350 50  0000 C CNN
F 1 "100" V 5000 5200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4930 5200 50  0001 C CNN
F 3 "~" H 5000 5200 50  0001 C CNN
F 4 "C17408" H 5000 5200 50  0001 C CNN "LCSC"
	1    5000 5200
	0    1    1    0   
$EndComp
Wire Wire Line
	4550 5000 4850 5000
Wire Wire Line
	4850 4900 4550 4900
Wire Wire Line
	4550 5200 4850 5200
$Comp
L power:GND #PWR0125
U 1 1 5F1B8868
P 3750 2600
F 0 "#PWR0125" H 3750 2350 50  0001 C CNN
F 1 "GND" H 3755 2427 50  0000 C CNN
F 2 "" H 3750 2600 50  0001 C CNN
F 3 "" H 3750 2600 50  0001 C CNN
	1    3750 2600
	1    0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TP1
U 1 1 5F23CFEB
P 3800 1950
F 0 "TP1" H 3742 1976 50  0000 R CNN
F 1 "TestPoint" H 3742 2067 50  0000 R CNN
F 2 "TestPoint:TestPoint_THTPad_D1.5mm_Drill0.7mm" H 4000 1950 50  0001 C CNN
F 3 "~" H 4000 1950 50  0001 C CNN
	1    3800 1950
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0126
U 1 1 5F25ACB8
P 3800 1950
F 0 "#PWR0126" H 3800 1800 50  0001 C CNN
F 1 "+5V" H 3815 2123 50  0000 C CNN
F 2 "" H 3800 1950 50  0001 C CNN
F 3 "" H 3800 1950 50  0001 C CNN
	1    3800 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:R R7
U 1 1 5E91FFA8
P 5000 5000
F 0 "R7" V 4950 5150 50  0000 C CNN
F 1 "100" V 5000 5000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 4930 5000 50  0001 C CNN
F 3 "~" H 5000 5000 50  0001 C CNN
F 4 "C17408" H 5000 5000 50  0001 C CNN "LCSC"
	1    5000 5000
	0    1    1    0   
$EndComp
$Comp
L Connector:TestPoint TP2
U 1 1 619DD861
P 4400 2500
F 0 "TP2" H 4342 2526 50  0000 R CNN
F 1 "TestPoint" H 4342 2617 50  0000 R CNN
F 2 "TestPoint:TestPoint_THTPad_D1.5mm_Drill0.7mm" H 4600 2500 50  0001 C CNN
F 3 "~" H 4600 2500 50  0001 C CNN
	1    4400 2500
	-1   0    0    1   
$EndComp
$Comp
L power:+5V #PWR0107
U 1 1 619DDD41
P 4400 2500
F 0 "#PWR0107" H 4400 2350 50  0001 C CNN
F 1 "+5V" H 4415 2673 50  0000 C CNN
F 2 "" H 4400 2500 50  0001 C CNN
F 3 "" H 4400 2500 50  0001 C CNN
	1    4400 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 5200 6500 5200
Wire Wire Line
	5450 5000 6500 5000
Wire Wire Line
	5450 4900 6500 4900
$Comp
L Connector:Conn_01x25_Male J1
U 1 1 610F184D
P 2450 3900
F 0 "J1" H 2558 5281 50  0000 C CNN
F 1 "Unicomp Pressfit" H 2558 5190 50  0000 C CNN
F 2 "common:mini-pressfit" H 2450 3900 50  0001 C CNN
F 3 "~" H 2450 3900 50  0001 C CNN
	1    2450 3900
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 610F6155
P 5300 5200
F 0 "D1" H 5293 5417 50  0000 C CNN
F 1 "LED" H 5293 5326 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 5300 5200 50  0001 C CNN
F 3 "~" H 5300 5200 50  0001 C CNN
	1    5300 5200
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D2
U 1 1 610F6BF2
P 5300 4900
F 0 "D2" H 5293 5117 50  0000 C CNN
F 1 "LED" H 5293 5026 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 5300 4900 50  0001 C CNN
F 3 "~" H 5300 4900 50  0001 C CNN
	1    5300 4900
	-1   0    0    1   
$EndComp
$Comp
L Device:LED D3
U 1 1 610F764A
P 5300 5000
F 0 "D3" H 5293 5217 50  0000 C CNN
F 1 "LED" H 5293 5126 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 5300 5000 50  0001 C CNN
F 3 "~" H 5300 5000 50  0001 C CNN
	1    5300 5000
	-1   0    0    1   
$EndComp
Wire Wire Line
	4550 5100 6500 5100
Wire Wire Line
	4550 5100 4550 5000
Connection ~ 4550 5100
Connection ~ 4550 5000
Wire Wire Line
	4550 5000 4550 4900
Wire Wire Line
	4550 5200 4550 5100
$Comp
L Connector_Generic_MountingPin:Conn_01x16_MountingPin J2
U 1 1 61104C29
P 2850 3400
F 0 "J2" H 2938 3314 50  0000 L CNN
F 1 "R" H 2938 3223 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x16_P2.54mm_Vertical" H 2850 3400 50  0001 C CNN
F 3 "~" H 2850 3400 50  0001 C CNN
	1    2850 3400
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic_MountingPin:Conn_01x12_MountingPin J3
U 1 1 61106193
P 2850 4800
F 0 "J3" H 2938 4714 50  0000 L CNN
F 1 "C" H 2938 4623 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x12_P2.54mm_Vertical" H 2850 4800 50  0001 C CNN
F 3 "~" H 2850 4800 50  0001 C CNN
	1    2850 4800
	1    0    0    -1  
$EndComp
$EndSCHEMATC
