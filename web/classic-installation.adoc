= M-Star Classic installation

THESE PAGES ARE UNDER CONSTRUCTION.  THE HARDWARE IS NOT YET SHIPPING.

Installing a M-Star Classic is simple enough that you can do it even if you
are not experienced at tinkering with computers.  Helpfully, Model
Ms are built like tanks; most of the internals are so rugged that you
would have to do actual work to damage them.

Nevertheless, the plague of lawyers is such that we must include
the following disclaimer:

WARNING: The M-Star Classic developers offer NO WARRANTY on the correct
operation of the M-Star Classic with your keyboard hardware, or against any
damage caused by M-Star Classic installation.  You modify your keyboard AT
YOUR OWN RISK of rendering it inoperable.

It is always good to have a wingman when doing an installation like
this. Your wingman can read the instructions to you as you work and
discuss them with you until you're sure you understand what to do.

With only one exception - which won't come up 99% of the time, and is
clearly marked - all these steps are nondestructive and
reversible. Save the parts you remove; in the (very unlikely) event
that the installation fails, it should be possible to reverse the
steps and reinstall your factory controller.

Test your keyboard before you begin dissassembling.  If you come to
think you have a failed upgrade and you didn't explicitly test
beforehand, you'll really wish you had done so.

The pictures in these instructions are of the most common version
of the Model M, the 1391401.  Your M will probably look a lot like these
pictures, but don't panic if it doesn't exactly match them.
The text directions are here to keep you out of trouble.

== Terminology and anatomy

There are some terms you need to know when reading these instructions.
Without them, we'd have to repeat a lot of confusingly similar
descriptive phrases, making them more difficult to follow.

Terms for some things you can see from the outside:

cover::
   The top half of the Model M's case. The part the keys protrude through.

pan::
   The bottom half of the Model M's case.

lock lights::
   Your Model M probably has three LEDs labeled NumLock, CapsLock. and
   ScrollLock; most do, and this means it was made to be used with a
   personal computer.

terminal M::
   A Model M without lock lights.  These were made to be used as
   terminals on various IBM computers other than PCs.  These are
   relatively uncommon, but if yours is one the M-Star Classic still
   has you covered.

SDL::
   Shielded Data Link. It's a kind of plug standard that looks like an
   oversized Ethernet plug. Your Model M may have a female SDL jack
   exposed on the rear surface of the pan.  If so, it came with an SDL
   to AT or PS/2 cable that plugs into the jack on one and some
   now-obsolete computer ports on the other.

fixed-cable::
   Describes a Model M that doesn't have an exposed SDL jack. Instead
   a cable issues directly out of the rear wall of the pan.

jack aperture::
   The hole in the rear wall of the pan that exposes the SDL jack to
   the exterior of the case.  Almost every Model M has one, but some hide
   it.  What is hidden shall be revealed.

exit notch::
   Some Model M variants don't have a jack aperture, but instead have
   a notch cut unto the upper rear edge of the pan for a fixed cable
   to exit through.

Here are some things you won't be able to see until you open the case:

include::innerparts.inc[]

Molex plug::
   Molex is a brand name for a family of internal connectors used in a
   lot of electronics. A common alternate term for these is "JST
   plug". There are many different varieties of Molex connectors, but
   in these instructions we mean a particularly common one with 4
   leads enclosed in a rectangular housing of white, semitranslucent
   plastic. There are male and female versions.  Image
   <<molex-and-mickey,here>>.

mickey::
   A name we made up for a small plastic part found in fixed-cable
   Model Ms, because it looks rather like the ears of a cartoon mouse.
   Boringly, it turns out to have the actual name "cable retainer".
   Image <<molex-and-mickey,here>>.

== Things you will need

* A thin-walled 5.5mm or 7/32" hex driver. This is the size required
  for the case screws on your model M. Find online with "5.5mm nut driver".

* Small container to keep screws and bolts in so they don't get lost.

* A pair of needle-nosed pliers (if your M doesn't have an SDL jack).

* A 5mm flat-bladed screwdriver (if your M doesn't have an SDL jack).

* An insulating, anti-static pad. A rubber mousepad or wrist-rest is
  ideal for this. A piece of cardboard or paper is passable.

* A USB 2.0 full-sized B cable of the kind commonly used with printers and
  scanners (for all variants other than round-top M122s).

* A USB micro-B cable (for round-top M122s) and optionally a
  small cable tie.

* Optional: a piece of mounting tape (foam tape with adhesive on both
  sides) small enough to fit under the M-Star Classic.

Choose the other end of your cable to match the ports on whatever
machine you want to connect to - usually this will be A, perhaps
sometimes C. The rest of these instructions are indifferent about
this.

The M-Star Classic has a full-sized female B jack. If your pan has a jack
aperture, your cable needs to have a full-sized male B plug to match
it, not mini- or micro-B. Note that you do *not* want the SS-B version
of a B plug with extra leads in the connector!  Usually these B cables
have a USB A plug on the other end (as in the images below), but get
whatever suits your computer's ports.

[grid="cols", align="center", width="50%"]
|====================================================================
| image:USB2.jpg[]             | image:USB3.jpg[]
| Yes! This [green]#WILL WORK# | No! This is SS-B and [red]#WILL NOT WORK#
|====================================================================

The M-Star Classic also has a female micro-B connector.  This is for use with
round-top M122s and any other types that have only an exit notch in
the upper edge of the pan rather than a jack aperture. In this case
you're going to need a micro-B USB cable rather one with a full-sized
B jack printer cable.

== First step: Smoke-test the M-Star Classic controller

include::smoketest.inc[]

== Second step: Open the case

You will need to open the case. Unplug your M, turn it upside-down,
and find the case screws on the bottom of the pan near the rear
edge.

.Finding the case screws
image::facedown.jpg[]

In this image, the case screws are near the rear edge of the keyboard
(uppermost in the picture) inside those four wells.

The specialized tool you need to remove them is shown sitting on top
of the manufacturing label. Note that a conventional 5.5mm hex driver
is likely to have walls so thick that it won't fit in the wells.

Once you've loosened the screws you may find you need to turn the
keyboard right-side up again so they will fall out of the wells.
Do this well away from the edge of your worktable, slowly, so as
not to lose any screws. Put the extracted screws in a safe place.

(If you do lose any screws, don't panic. They are a common type
called a "#6 sheet metal screw" and can be had for pennies at just
about any hardware store.)

Now a thing not to do: don't try to disassemble the rest of the
keyboard in an inverted position.  Turn it right-side up first.

On many stock vintage Ms you can get away with diassembling inverted,
because the controller easily falls off the pegs.  But your controller
may be clipped in place, or if has been field-serviced it may be stuck
down with mounting tape. If so, and you try to disassemble inverted,
you will probably pull the FFCs out of their sockets.

To avoid putting wear on the cable ends and connectors, turn the
keyboard right-side up.  Lift the cover off first; you should be
looking at the keys and (behind them) the plate assembly.  You should
be able to see the top edge of the metal backplate protruding from
behind the plastic barrel plate in the front.

Lift the plate assembly just enough that you can pry the controller
off the pan *before* you try lifting the plate assembly all the way
out.

.Cover off
image::faceup.jpg[]

Notice the little daughterboard with the lock lights on it at the
upper right of the "Cover off" image.  That's what you want to see,
though on some very old Model Ms there may be discrete wires running
to the daughterboard rather than a 4-lead ribbon cable.  Or there may
be no lock lights at all and no daughterboard - that's a terminal M.

What you don't want to see is this:

.If you see a PCB like this, stop!
image::notgood.jpg[]

If you have a PCB like this, stop!  We're sorry, but this means you
have a late Lexmark or Unicomp Model M which the M-Star Classic will not fit.
Screw the cover back on and carry on with your life.

== Third step:  Unplug the ribbon cables

Now that you have verified that your controller can be
replaced by the M-Star Classic, turn the case around until you're looking at
the rear of the keyboard.

We recommend that you take a photograph of the rear view with cables
in place before continuing, so you'll be able to revert correctly if
the installation fails.

You will probably see two or three ribbon cables running from just over the top
of the backplate down to sockets in a PCB sitting underneath it in the
pan.

.Model M from the rear, showing the ribbon cables.
image::classic-rearview.jpg[]

This first rear view shows a common arrangement in older Model Ms -
one 16-lead socket, one 8-lead, and one 4-lead.  There are other
variations, but the 16-lead socket will always be present.  If your M
was made after 1992, you may instead see a single 12-lead socket in
place of the 8-lead and 4-lead pair. On some very early Ms with lock
lights the connector is a discrete 4-wire header rather than a ribbon
cable socket, and it may be set at right angles to the others.

Alternatively, the PCB may be bolted to the backplate on its
underside.  Only terminal Ms (no lock lights) were assembled this way.
If you have a terminal M you may see something like this:

.PCB mounted on the M backplate
image::plate-mount.jpg[]

In the second rear view (the terminal M), the socket arrangement is
16-8; there is no 4-lead socket because the lock lights it would serve
are not there.  Note that with this kind of mounting the ribbon
connectors will be on the *underside* of te board, not the top.

Either way, you will need to detach the ribbon cables connecting the
plate assembly to the PCB.  The next move will make that easier.

There should be two posts sticking up from the pan near the interior
left rear and right rear corners, snapped into holes in the metal
backplate.  You can see these in the previous "Cover off" image.

Disengage the backplate from the posts by pulling it carefully upwards.
Some Model Ms have a small notch in the rear side of each post to hold the plate
in place, so it won't just lift off.  You'll have to use just a little
force to disengage the plate from the notches.

Let the backplate settle back onto the posts so it sits on top of
them without re-engaging the posts. You may be able to do the next step
- actually pulling the ribbon cables out of their connectors - with
mo more fuss.  If you don't have quite enough room to get purchase on
them, lift the rear edge of the backplate an inch or so with one
hand to get the clearance you need for the other.

Only friction keeps the ribbon cabless in the ribbon sockets; a slow, firm pull
with your fingers should get them out. While it might seem tempting to use the
needlenose pliers for this, those metal jaws can easily damage them
and we strongly recommend against it.

If you have a very old M with a 4-wire connector for the LEDs, and you
can reach that to pull it free, do that too.

You have now disconnected the rest of the keyboard from its
electronics and can proceed to the next step.

== Fourth step: Lift away the plate assembly

Lift the plate assembly off the pan.  Do it slowly, as there might
be one connecting cable remaining that you shouldn't put strain on.

If the PCB is bolted to the backplate's underside, the cable going out
the back of the keyboard through the rear wall of the pan will end in
a male Molex plug, which is seated in a female Molex receptacle on the
underside of the PCB.  Pull that plug out.

While you might be able to do this with unaided fingers, gripping the
edges of the plug with your needlenose pliers makes it easier.  Male
Molex plugs have a pair of projecting tabs on their upper edges that is
meant to make this easier.  Note: do not grip the receptacle on
the bottom, you don't want to rip that off the PCB!

If you have a very old M with a 4-wire connector for the LEDs, the
connector may be in a location that was not reachable when you
disconnected the ribbon cables and lifted the platre assembly off the
pan. If so, disconnect that now.

When you first see the upper surface of the pan it may be littered
with tiny plastic disks, some with short stems on them.  These
are the heads of what used to be rivets holding the plate assembly
together.  Over time the rivets grow brittle and the heads start
to shear off.

A few busted rivets - up to a dozen or so - are generally tolerable.
If you lose too many, sections of the keyboard can become mushy and
eventually fail to register keystrokes at all.  This can be fixed with
a procedure called a "bolt mod". Feed "Model M bolt mod" to a search
engine to learn more.

== Fourth step:  Remove the PCB

If your M has an SDL jack, you can usually just lift the
controller board off the pegs, and that will finish this step.
The pan may have retaining clips that engage the PCB; if so, flex
those outwards a little to get it free.

=== Beware the friction washer

There might be one complication. While it's not usual, some Ms have
been shipped with one-way friction washers as hold-downs for the PCB.
These are thin circlets of spring steel with inward-pointing prongs
shaped so that they can be pushed down around the mounting peg, but if
you try to pull them back up the prongs dig in.

.One-way friction washers in a New Model M SSK
image::friction-washers.jpg[]

Trying to force a friction washer upwards and off will just destroy
the mounting peg, because the steel is stronger than the plastic.
It's better to destroy the friction washer instead.  This is the only
irreversible step in the upgrade.

Fortunately, those hold-downs aren't really needed unless your
keyboard is subjected to sufficiently regular and violent mechanical
shocks that you should probably reconsider your lifestyle choices.

You can take out the friction washers with a pair of tin snips, aka
metalcutting dikes. An electrician's sidecutter will probably do,
though it will take more effort.  Attack the thinnest sections of the
circlet and cut at it until it loses its lock on the peg.

Alternatively, you may be able to use a very small flat-bladed
screwdriver or the tip of a knifeblade to bend the prongs of the
washers upwards so that they no longer grip the pegs.  Be gentle, as
breaking those pegs can make the keyboard unusable.

=== Fixed-cable Ms

If you have a fixed-cable M, deinstallation is a little trickier. You
should see something like this:

[[molex-and-mickey]]
.Molex and mickey on a fixed-cable M.
image::cable-support.jpg[]

Unscrew the grounding bolt over on the right; you'll need a small
flat-bladed screwdriver for that (about a 5mm blade).  Then pull the
vertical Molex plug out of its socket. Again, that's difficult with
fingers alone, you will probably want needlenose pliers.

Next, the mickey needs to be lifted off the mounting pegs. Use the
flat-bladed screwdriver to pry at the bottom if you need to, but it
should come out easily.

With the the mickey gone and the Molex plug disconnected, you should be able
to pull the cable out though the exit hole that is revealed - the same
one used in earlier M versions for the female SDL jack.

Once all cables have been detached, remove the PCB. Usually you will
do this simply by lifting it off the mounting pegs and out of the pan.
Again, the pan may have retaining clips that engage the PCB; if so,
flex those outwards a little to get it free.

=== Terminal Ms with plate-mounted PCBs

If the PCB is bolted to the backplate, just undo the bolts and take it
off.

You won't need the bolts again; we recommend you put them in the
through-holes of the old controller board in case you ever want or
need to reinstall it.

You'll see a mickey as in the last image above this text, but
without the PCB beneath it.  Lift the mickey off the pegs and pull the
cable out though the jack aperture.

== Fifth step:  Attach the ribbon cables to the M-Star Classic.

include::insertion1.inc[]

You have an ideal starting position when the ribbon cable ends
are resting directly on the connectors they're to plug into.
You want them to go in as vertically as possible - bending is
not good for the ribbon cables.

Order is significant.  If you have a 4-pin LED connector,
do that one last as it will be easiest.  Otherwise, if you
are right-handed work left to right so your hand doesn't have
to collide with a ribbon cable you've already plugged in.
If you're left-handed go in the other direction.

If you have an 8-lead ribbon cable and a a four-wire connector that is
*not* a ribbon cable remaining, insert the 8-lead ribbon cable in your
12-lead socket at the end nearest the 16-lead connector.  The 4
rightmost pins in the 12-pin socket will be left unconnected.

include::insertion2.inc[]

.What ribbon cables look like when properly seated
image::ribbonfit.jpg[align="center"]

This is what ribbon cables look like when they're properly inserted.

//FIXME: Need image of a 16-12 installation here

image::installed-16-8-4.jpg[]

There are two ways it can be inserted; do it so the cable right-angles
towards the rear of the pan (and toards you), not the front.

//FIXME: need image of installation with discrete LED wires here

If you're installing in an M122 rather than an M101, the PCB needs to
project off to the right of the jack rather than the left. The
following image is *not* of a 1391401.

image::j-gf-in-m122.jpg[]

== Sixth step:  Testing and LED configuration

include::testing.inc[]

If you see characters echoing but the lock lights don't flash, don't
despair. The M-Star Classic defaults to feeding the lock-light LEDs
3.3v; many older Model Ms need 5v, especially those with 16-8-4 FFCs.
Find the indicated jumper, fix this, and try again.

[grid="cols", align="center", width="50%"]
|====================================================================
| image:jumper3v.jpg[]       | image:jumper5v.jpg[]
| Jumper in 3v (up) position | Jumper in 5v (down) position
|====================================================================

End this step by unplugging your USB cable from the M-Star Classic.

== Seventh step: Seat the shroud

Settle the plastic shroud provided with your M-Star Classic over the jack
housing on the M-Star Classic.  Its purpose is to seal the jack exit so
contaminants can't get at the PCB or keyboard internals.

If you have mounting tape, there is room between the angled surface on
the shroud and the backplate to stick a small piece of the tape
there. Peel off the backing on the bottom side but on the top; the
idea isn't to stick the tape to both shroud and backplate, just pad
it securely in place when the backplate is in its installed position.

== Eighth step: Seat the M-Star-Classic and put the plate assembly back in place

The next image shows the mounting pegs you're looking for after the
old PCB has been removed.  These will hold the M-Star Classic and its USB
jack in place.

.The mounting pegs near the SDL jack
image::mounting-pegs.jpg[]

If the pan does not have retainer clips sized to engage the M-Star Classic,
and you have mounting tape, you can use the tape as another way to
stabilize the PCB in place.  (This is a field-expedient modification
that was often done by IBM service technicians.)

To do this, peel off the backing on one side of the tape and press it
onto the bottom of the M-Star Classic. Then peel off the backing on the
exposed side.

include::reseat.inc[]

If you put mounting tape under the M-Star Classic, press down lightly on it
until you feel the foam compress slightly against the pan surface.
That should make the adhesive take hold.

== Ninth step: Final reassembly

If you're upgrading a keyboard that has only a cable exit notch rather
than a jack aperture, plug your cable into the jack on the PCB and
route it through the exit notch before reassembling.  If you have a
small cable tie, consider looping it around the cable just inside the
exit notch and pulling it good and tight around the cable right up
against the back wall; this is useful strain relief.

Assuming all went well, push the rear pair of holes in the backplate
down over the posts so they snap into the retaining notch. You might
need to wiggle the plate assembly a bit if it isn't sitting square.

Put the cover back on, invert the keyboard and put the case screws
back in.  You're done.

// end
