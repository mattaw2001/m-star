Plugging in one of these ribbon cables takes a little work. The  
connectors inside the resistence are springy, meant to clamp the
ribbon end in place.  This means they resist opening up to let the
ribbon cable in when you're inserting it.

What you need to do is grasp the ribbon close enough to the end that
the little bit out front of your fingers is essentially rigid, then
push the leading edge through the resistance of the contacts.  You'll
feel a hard stop when the ribbon edge hits the bottom of the socket.

Again, it might seem tempting to use the pliers, but that's not a good
idea. Not only can the metal jaws damage the FFC, but feeling your
way with the pliers makes it more difficult to be sure of the stop
at the end of insertion.  Fingers are best for this.

WARNING: The kind of ribbon sockets used by factory Model M
controllers and M-Star replacements are only rated for 10 insertions
before the finish on the inner contacts might wear to the point when
they no longer establish a good conductive path for the signal.  Worse
than that, the ribbon ends get significant wear on each insertion; if
you put them through too many you might make them unusable too.  It
might be possible to repair damaged ribbon ends with a conductive-ink
pen, but it's better to avoid potentially ruining the ribbon and the
membrane with it.

So, minimize ribbon removal and insertion operations as much as
possible.  If all goes well this upgrade should require just one
extraction per connector (which you've already done) and one
insertion; try hard to keep it that way.
