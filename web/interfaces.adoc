= Interfaces to the Model M

This page collects information on how Model M controllers mate with
Model M keyboards.  These interfaces are the main constraints on the
design of a replacement controller.

== Terminology

pan::
    Bottom half of a Model M case shell.

cover::
    Top half of a Model M case shell.

press-fit controller::
    Smaller controller used in Unicomps and post-1995 Lexmarks

FFC::
    Flexible Flat Cable, aka a ribbon cable.  Note that connector
    pitch used on Model M controller FFCs is 0.1in = 2.54mm rather than
    the 1mm that has since become typical.

All uses of "left" and "right" assume a view from the
front.

== Controller dimensions and membrane interfaces

We've assigned type names to controllers that IBM never used, because they
persisted across multiple part numbers.  Part numbers shown are
representative but not exclusive

Year ranges may be too narrow, there are gaps in the data.  The
hotlinks go to images.

[options="header", width="75%"]
|=========================================================
|Tag                 | part #   | Years     | Dimensions | Connectors        | LEDs    | Output
|<<bravo,Bravo>>     | 1390131  | 1985-1987 | 170x50mm   | 16-8 FFC socket   |5v wired | SDL jack
|<<generic,Charlie>> | 1391401  | 1987-1989 | 170x50mm   | 16-8-4 FFC socket |5v FFC   | SDL jack
|<<delta,Delta>>     | 1391403  | ?-1991-?  | 150x38mm   | 16-8-4 FFC socket |5v FFC   | SDL jack
|<<generic,Echo>>    | 1391401  | 1990-1993 | 150x38mm   | 16-12 FFC socket  |?        | SDL jack
|<<generic,Foxtrot>> | 1391401  | 1992-1993 | 170x50mm   | 16-12 FFC socket  |?        | SDL jack
|<<golf,Golf>>       | 1392934  | 1987-1992 | 150x38mm   | 16-8 FFC socket   |None     | SDL jack
|<<generic,India>>   | 82G2383  | 1993-1995 | 170x50mm   | 16-12 FFC socket  |?        | 4-pin Molex
|<<juliet,Juliet>>   | 52G9758  | ?-1993-?  | 164x70mm   | 16-12 FFC socket  |?        | 4-pin Molex 
|<<kilo,Kilo>>       | 42H12942U| 1995-     | 70x42mm    | 25-pin edge connector|3.3v  | 4-pin Molex
|<<lima,Lima>>       | 13H6705  | ?-1996-?  | 220x50mm   | 16-12 FFC socket  |?        | 4-pin Molex,
  trackpoint and button connectors.
|<<mike,Mike>>       | 1397000  | ?-1998-1992-? | 170x50mm | 20-8-4 FFC socket|?       | SDL jack
|=========================================================

The Bravo shipped with two different but physically compatible pin
connector variants for the LED header.

The Echo has the 4-lead FFC socket for the LEDs at right angles to
the others, like some Bravos.

The Golf variant was made for SSKs and thus has no lock light.
Similar 16-8 comntrollers on 170x50mm PCBs were made for 101-key
terminal Ms.

The 4-pin Molex connector exported PS/2. The pins on the India were
vertical, on the Kilo horizontal pointing right, on the Lima pointing
to the rear.

The Kilo has a press-fit controller, so called because it has an edge
connector that is pressed against pads on the upper edge of the sensor
membranes. It has integral LEDs and sits directly beneath the lock
light panel under the upper right corner of the cover. This was the
style Lexmark and and Unicomp shipped before Unicomp went to a card
with the same size and membrane connector but USB output.

The Lima is a controller used in MaxiSwitch Ms with a trackpoint and
mouse buttons.  The extra FFC connector near the front end the
six-wire connector over at the right edge handle those.  Minor
variants have been observed that are 223mm long.

== Retainer catches

Some M pans have internal catches to engage and fully support the
controller.  Early versions have two catches positioned to engage
50mm-wide controllers.  Later versions have three catches, the third
positioned to engage 38mm-wide controllers.

The manufacturing change probably happened in 1993.  52G9758s dated
that year have been observed with both 2 and 3 catches.

This means that controllers with a 38mm wide form factor can fit in a
case designed for the larger 1670x50mm PCBs, but not vice-versa.

When catches did not match controller, a common field expedient by IBM
service techs was to put double-sided mounting tape on the underside
of the PCB to stick it on the pan. Or, the 38mm catch was
sometimes deliberately sheared off so the pan would fit a 50mm-wide
controller.

Pans designed for use with the press-fit controller have no catches at
all.

== PCB and feature location constraints

All M cases other than the older "round-top" style of M122 have an
aperture for an SDL jack and mounting pegs for the PCB near it. On
fixed-cable Ms this hole is masked and does not become apparent until
a plastic shim interlocked with a strain reliever on the cable is
removed.

The mounting pegs for the PCB are located on either side of the jack
aperture. Most controller boards have through holes that go over those
pegs. On fixed-cable Ms the shim occluding the SDL aperture sits atop
the PCB on those pegs.  Some terminal Ms have the mounting pegs but do
not use them.

Round-top M122s have an exit notch for a fixed cable, with the PCB
bolted to the plate assembly.  More details on this follow.

=== M101

M101 pans from Bravo to Juliet have the following
features constraining the board size and location:

* The SDL aperture is located somewhat to the left of the board's
  vertical midline.

* Two support fins for the back plate, 223mm apart and placed
  symmetrically on either side of the vertical midline.  These
  constrain the long dimension of the PCB.

* Some pans have retaining clips that lock with the front edge
  of the board.  They may constrain its short dimension to 50mm or
  38mm, depending on the part number

Clearances:

* Between the support fins: 223mm

* Between the left edge of the PCB and the left-hand support fin: 6mm

* Between outer edge of right mounting pin and left end of 16-pin
  FFC socket: 9mm (approximate)

* Between right end of 16-pin FFC socket and left and of 8- or 12-pin
  FFC socket: 11mm (approximate)

Allowing the same clearance on the right-hand side as the PCB has to
the left, the maximum possible dimensions of a PCB to fit in every M101
variant works out to 211x38mm.  This is 41mm more than the largest
commonly-observed long dimension of 170mm.

The distance between the jack and the left end of the 16-pin
socket, and the distance between the right end of the 16-pin socket
and the left end of the 8- or 12-pin socket, are tagged "approximate"
because the shape of the FFC socket makes these tricky to
define unambiguously.

However, they are both <<spacing,critical>>.  Unless they replicate the separation
on Bravo through Juliet, they won't be in the right places for the
membrane sensor FFC ends to land and will cause damaging lateral
stress on the FFCs.

=== M122

There were two different styles of cover - one (earlier) more
rounded at the back edge, one (later) squared off.  The pans
on these are different - the round-tops have only an exit hole for a
fixed cable, while square-tops have a jack aperture occluded with a
strain-relief collar like later Lexmark M101s.

All square-top M122s observed in the wild have the same IBM part number
molded in near the upper right corner of the pan: 1393997.  This part
has an SDL aperture.

The 1393997 pan had a different interior layout than the pans of Bravo
through Juliet variants, with the SDL aperture on the center-line
rather than displaced leftward, and the support fins in different
locations. However, the mounting pegs are at the same locations
relative to the jack.

The location of the right-hand support fin on these pans would
interfere with the usual placement of a controller projecting to the
right from the jack, so in variants where the controller is
peg-mounted it projects to the *left* of the SDL aperture.  The
FFC ends from the membrane fall in this region and are the
other main constraint on where the controller can be placed.

The distance between the jack and the right end of the 8-pin
socket, and the distance between the left end of the 8-pin socket
and the right end of the 20-pin socket, are both <<spacing,critical>>.
Unless they replicate the separation on Mike, they
won't be in the right places for the membrane sensor FFC ends
to land and will cause damaging lateral stress on the FFCs.

Clearances:

* Between the support fins: 282mm

* Between left support fin and outer edge of left peg: 187mm

* Between right support fin and outer edge of right peg: 67mm

* Pan rear wall to nearest transverse rib: 85mm

Thus there is well over 50mm of clearance between the jack socket and
the nearest transverse rib on the pan.

There have been at least three three different variants of 122-key
Model Ms:

The classic "battleship" M122 exported a 20-8 FFC connector
pair. It was made for use with terminals, so no lock lights. The
controller was plate-mounted, though the mounting pegs in the pan were
still present. We haven't documented controller sizes or mount points
because any sane replacement would use peg mounting and the 38mm
width.

The "Mike" entry in the main table is the controller of the very rare
1397000. This variant of the M was a terminal keyboard manufactured by
Lexmark and was used for talking to an AT-286 front-ending an
AS-400. Unusually, it had lock lights (hence the 20-8-4 FFC cables).
The peg-mounted controller projects to the left.

Unicomp has made M122s with 1393997 pans, like the Mike, but which use a
variant of their Kilo press-fit controller; a representative model is the
UB40B5A from 2012.

=== Mounting styles

On M101s with locking lights, all controllers other than the Kilo engaged
the mounting pegs described in the previous section.  The Kilo
controller sits in the cutout at the upper right end of the barrel
plate where the LED daughterboard was on older variants.

Some terminal-M controllers are peg-mounted - this is true of 101-key
terminals and the newer "square-top" style of M122.  On older
round-top M122s the controller is instead bolted to the backplate, or
a rail attached to the backplate, and (as previously notd) the fixed
cable exits through a simple notch.

We have not recorded the sizes of plate-mounted terminal-M
controllers or the locations of their through holes on the controller
PCB, because terminal Ms still have the mounting pegs available to
secure a replacement PCB.

[[spacing]]
=== FFC cable issues

Be careful about moving the positions for the main FFC socket (the 16
and 8 or 12) relative to the mounting pegs. While the FFC cables
are very tolerant of having their landing site moved on the
front-to-rear axis of the case, they are extremely intolerant of
lateral displacement that forces them to land at even a slight angle.
A lateral shift of as little as 2-3mm can result in connection
problems and excessive wear on the cable ends.

"Excessive wear" is a serious problem because the FFC cable ends on
the Model M's sensor membranes can in the best case endure only a very
limited number of insertions before the conductive finish on the
contacts becomes damaged enough to cause continuity problems. This
makes the entire membrane unusable, though the damage can sometimes be
patched with a conductive-ink pen.

Unfortunately these positions are difficult to describe unambiguously.
Match your prototype up against a factory PCB to be sure you have the
spacing right.

The connector sockets are fragile too; the ones used on stock Model M
controller board are only rated for 10 insertions before failure.

== LEDs

Pre-Lexmark Ms used 5V LEDs.  These were wired to the controller in two different ways;
early by 4 discrete wires, later by a 4-lead FFC cable.

Lexmark changed over to 3.3V LEDs at some point after 1991 that has not yet been
precisely determined but was most likely during 1993.  The change may
not have been simultaneous at all three manfacturing sites.

== Keyboard matrix and connector pinouts

This is a pinout and matrix diagram for SSK Ms - the older IBM
version, not the Unicomp Mini-M SSK:

image::Layer_2_-_COLOR_MATRIX_COMBINED.jpg[]

We have a directory of scans of the Unicomp Mini-M matrix at
web/2021-model-m-unicomp-matrix-scan in the repository.

== Miscellanea

The Model M keyboard font is Helvetica Condensed

The range of blues IBM used is decribed
https://www.ibm.com/design/language/color/[here]; the blue the Model M
used was probably Cyan40 (33b1ff).

== Images

The following image shows most of the controller boards shipped in
Model Ms made for PCs (e.g. with lock lights) from 1985 to 1995
(before the "Kilo" Lexmark redesign that moved the board to under the
lock light panel).  Here's a key that maps from our interface types to
the top image:

|=========================
| Bravo   | Charlie | Foxtrot
| Charlie | Echo    | Foxtrot
| Charlie | Echo    | India
|=========================

[[generic]]
.Model M controller boards 
image::controllers.png[]

[[bravo]]
.Bravo variant in situ in a 1390131
image::1390131.jpg[]

[[delta]]
.Delta variant in situ in a 1391403
image::1391403.jpg[]

[[golf]]
.Golf variant in situ on an SSK
image::ssk-controller.jpg[]

[[juliet]]
.Juliet from a 52G9758
image::52G9758.jpg[]

[[kilo]]
.Kilo "press-fit" controller
image::notgood.jpg[]

[[lima]]
.Lima in a 13H6705
image::13H6705.jpg[]

[[mike]]
.Mike in an M122 pan (part 1393997)
image::1393997.jpg[]

There are many, many images of controller boards
http://clickeykeyboards.squarespace.com/model-m-gallery/comparision-of-model-m-internal-controllers-1987-1999/[here.]
The top image from this gallery comes from that site.

// end

