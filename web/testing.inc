We recommend having another keyboard plugged in as a backup
during testing.

Make sure your PCB is sitting on an insulating pad for this step.
You may get confusing results that look like it;s defective,
otherwise.

Plug your USB cable into both the keyboard and a computer. You should
see a red light go on, as you did in the first step.

Do what you need to do to bring up a program on your computer thst
will echo characters when you type to it - terminal emulator, Notepad,
whatever. Type "asdf".  If you see "asdf" echoed back at you, you win.

Searching on-line for "keyboard tester" will find you some useful web
pages.

If you don't get character echo, bear in mind that a last resort you
can put the old controller back in place - and go to our
link:diagnostics.html[diagnostics page].

Here are two other things to watch for:

* All three lock lights should flash sequentially left to right witin
  a second after the keyboard is plugged in.

* If you can light up the Num Lock and Caps Lock lights on the Model M
  with the Num Lock and Caps Lock on the backup keyboard, that's good

End this step by unplugging your USB cable from the M-Star Mini.
